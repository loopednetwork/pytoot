import json
import os
import requests
import socket
import sys

from urllib.parse import urlparse


# Define some file paths for now with the authentication information if it
# already exists on the filesystem.
_CLIENT_ID = ""
_CLIENT_SECRET = ""
_CLIENT_DATA_PATH = "/data/client_data.json"
_INSTANCE_FILE_REL_PATH = "/data/instance.txt"

def hostname_resolvable(hostname):
    """
    Validates if the given hostname is resolvable.

    Parameters:
        (string) for the hostname to check.

    Returns:
        (bool) indicating if the resolution was successful.
    """
    try:
        socket.gethostbyname(hostname)
        return True
    except socket.error:
        return False

def get_instance(dir_name):
    """
    Ensures that the user's Mastodon instance is known and at least resolvable.

    Parameters:
        (string) with the name for the base directory.

    Returns:
        (string) with the domain for the user's instance.
    """
    # Track if the value was changed in case we need to update the file or
    # write it fresh.
    domain_write_needed = False

    # Parse the instance information path.
    instance_path = parse_path(dir_name, _INSTANCE_FILE_REL_PATH, 1)

    # Load up the domain from the file or get it from the user.
    while True:
        if os.path.isfile(instance_path):
            # Read the file.
            with open(instance_path, "r") as file:
                instance_domain = file.readline().strip()

                # Verify if there's actual data.
                if instance_domain == "":
                    os.remove(instance_path)
                    continue
                else:
                    break
        else:
            domain_write_needed = True
            print("No instance found! Please enter the domain for your instance.")
            print(" -- Note: no \"https\" is necessary. --")
            instance_domain = input("> ")
            break

    # Validate the format of the domain.
    standalone_domain = urlparse(instance_domain).netloc
    if standalone_domain == "":
        standalone_domain = instance_domain
    if standalone_domain != instance_domain:
        domain_write_needed = True

    # Verify it can be resolved.
    if not hostname_resolvable(standalone_domain):
        print(f'Unable to resolve host: {standalone_domain}')
        print('Please validate the domain is correct and try again!')

        # Delete the file if it's there since it may have bad data.
        if os.path.isfile(instance_path):
            os.remove(instance_path)

        # Quit for now. Maybe go into a retry later?
        sys.exit(1)

    # Update the file if needed.
    if domain_write_needed:
        with open(instance_path, "w") as file:
            file.write(standalone_domain)

    # Return the instance to the caller.
    return standalone_domain

def parse_path(full_path, relative_path, offset):
    """
    Function to take a current full path and append a relative path to the end,
    removing any offset of directories.

    Parameters:
        (string) for the full path of the current file
        (string) for the relative path of the new file
        (int) for the number of directories to traverse up the chain

    Return:
        (string) with the new fully qualified path to the desired file
    """
    full_path_list = full_path.split('/')
    return '/'.join(full_path_list[:offset * -1]) + relative_path

def get_token(url, client_id, client_secret):
    """
    Gathers a token from Mastodon which can be used for other activities going
    forward in the execution of the overall application.

    Parameters:
        (string) for the URL to POST to for a token.
        (string) for the client ID required for a token.
        (string) for the client secret required for a token.

    Returns:
        (string) with the token value.
    """
    data_dict = {
        "client_id": client_id,
        "client_secret": client_secret,
        "redirect_uri": "urn:ietf:wg:oauth:2.0:oob",
        "grant_type": "client_credentials"
    }
    token_reponse = requests.post(url, data=data_dict)

    # Parse the response.
    if token_reponse.status_code == 200:
        token_response_dict = json.loads(token_reponse.content)
        return token_response_dict.get('access_token')
    else:
        print(f'Error Getting Token: {token_reponse.status_code}')
        sys.exit(1)

def verify_credentials(url, token):
    """
    Function to verify the credentials that are known prior to responding to
    the caller with them so we know they are usable.

    Parameters:
        (string) with the url to GET the verification to.
        (string) with the currently known token.

    Return:
        (bool) with whether or not the credentials are valid.
    """
    print(f'Verifying Token at Url: {url}')
    verify_response = requests.get(url, headers={"Authorization": f'Bearer {token}'})

    if verify_response.status_code == 200:
        print(f'Successfully Verified Token: {json.loads(verify_response.content)}')
        return True
    else:
        print(f'Error verifying token: {verify_response.status_code}')
        return False

def authenticate():
    """
    Ensures that the client has been authenticated. This function is typically
    going to be the first call made by the application at every launch to
    either get a new token or leverage an existing one.

    Parameters:
        (none)

    Return:
        (bool) indicating whether or not the client is authenticated.
    """
    # Get the current directory name.
    directory_name = os.path.dirname(__file__)

    # Get the instance currently in use.
    instance_domain = get_instance(directory_name)

    # Parse together the path to the JSON for client data.
    client_json_path = parse_path(directory_name, _CLIENT_DATA_PATH, 1)

    # Check if the data file exists.
    if os.path.isfile(client_json_path):
        # Read from the file.
        with open(client_json_path, "r") as file:
            client_details = json.load(file)
            _CLIENT_ID = client_details.get('client_id')
            _CLIENT_SECRET = client_details.get('client_secret')
    else:
        # Authenticate and write to the file.
        form_dict = {
            "client_name": "PyToot",
            "redirect_uris": "urn:ietf:wg:oauth:2.0:oob",
            "scopes": "read write follow push",
            "website": "https://borked.digital"
        }
        client_url = f'https://{instance_domain}/api/v1/apps'
        app_response = requests.post(client_url, data=form_dict)

        if app_response.status_code == 200:
            # Parse the response and write it to a file for later.
            response_dict = json.loads(app_response.content)
            print(f'App Registration Response: {response_dict}')
            _CLIENT_ID = response_dict.get('client_id')
            _CLIENT_SECRET = response_dict.get('client_secret')

            storage_dict = {
                "client_id": _CLIENT_ID,
                "client_secret": _CLIENT_SECRET
            }

            with open(client_json_path, "w") as file:
                file.write(json.dumps(storage_dict))
        else:
            print(f'Error retrieving client ID and secret: {app_response.status_code}')
            sys.exit(1)

    # Exchange the client ID and secret for a token.
    # This should be changed to auth the USER!!
    token_url = f'https://{instance_domain}/oauth/token'
    access_token = get_token(token_url, _CLIENT_ID, _CLIENT_SECRET)

    # Verify the credentials prior to returning them.
    verification_url = f'https://{instance_domain}/api/v1/apps/verify/credentials'
    if verify_credentials(verification_url, access_token):
        return access_token
    else:
        return ""

