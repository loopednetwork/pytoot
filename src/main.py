#!/usr/bin/env python3
import os
import sys

from auth import authenticate

def main():
    # First check if there is a known token.
    access_token = authenticate()
    if not access_token == "":
        print(f'Access Token: {access_token}')
    else:
        print('Did not receive valid access token!')
        sys.exit(1)

if __name__ == "__main__":
    main()

